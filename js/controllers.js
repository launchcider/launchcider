'use strict';

function toName(num){
	if(num < 10)
		return '00' + num;
	else
		return '0' + num;
}

function rand(max){
	return  Math.floor((Math.random() * max)); 
}

function drawChart() {
	var lineChartData = {
		labels : ["January","February","March","April","May","June","July"],
		datasets : [
			{
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,1)",
				pointColor : "rgba(151,187,205,1)",
				pointStrokeColor : "#fff",
				data : [28,48,40,19,96,27,100]
			}
		]
	}
	
	var lineCtx = document.getElementById("myChart1").getContext("2d");
	var myNewLineChart = new Chart(lineCtx).Line(lineChartData);
	
	var pieChartData = [
		{
			value: 30,
			color:"#F38630"
		},
		{
			value : 50,
			color : "#69D2E7"
		}
	]
	
	var pieCtx = document.getElementById("myChart2").getContext("2d");
	var myNewPieChart = new Chart(pieCtx).Pie(pieChartData);
	
	var barChartData = {
		labels : ["January","February","March","April","May","June","July"],
		datasets : [
			{
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,1)",
				data : [65,59,90,81,56,55,40]
			}
		]
	}
	
	var barCtx = document.getElementById("myChart3").getContext("2d");
	var myNewBarChart = new Chart(barCtx).Bar(barChartData);
	
	var doughnutChartData = [
		{
			value: 30,
			color:"#F7464A"
		},
		{
			value : 50,
			color : "#E2EAE9"
		},
		{
			value : 100,
			color : "#D4CCC5"
		},
		{
			value : 40,
			color : "#949FB1"
		},
		{
			value : 120,
			color : "#4D5360"
		}

	]
	
	var doughnutCtx = document.getElementById("myChart4").getContext("2d");
	var myNewDoughnutChart = new Chart(doughnutCtx).Doughnut(doughnutChartData);
}

/* Controllers */

angular.module('myApp.controllers', [])
  .controller('mainController', ['$scope', function($scope) {

	$scope.images = [];


	var options = [];
	options['Budget'] = ['$', '$$', '$$$'];
	options['Size'] = ['Compact', 'Medium', 'Large', 'Expansive'];
	options['Room'] = ['Kitchen', 'Bath', 'Bedroom', 'Living', 'Dining', 'Outdoor', 'Kids', 'Home Office'];
	options['Style'] = ['Contemporary', 'Eclectic', 'Morden', 'Traditional', 'Asian', 'Beach Style', 'Industrial'];

	$scope.filterTypes = ['Budget', 'Size', 'Room', 'Style'];
	$scope.filters = [];
	$scope.selectedFilter = [];
	$scope.filterStatus = [];
	$scope.filterObj = {};
	$scope.filterObj.Reaction = 'None';

	var names = ['Catheryn Rubenstein','Raymon Crisp','Bernard Stanford','Geraldo Rubalcava','Isadora Moschella','Raymonde Smolka','Jenae Doak','Shawnna Wolanski','Genaro Durkee','Jospeh Holloman','Shalonda Bissette','Lesley Castillo','Marlana Richison','Christian Godwin','Layne Cloe','Keneth Ketron','Ingrid Pendergrast','Catina Harlan','June Cuadrado','Dwight Reamer'];


	for(var i=1; i<100; i++){
		var image = {};
		image.ID = i;
		image.Url = 'img/gallery/images_' + toName(i) + '.jpg';
		
		for(var j=0; j<4; j++){
			var filterType = $scope.filterTypes[j];
			var option = options[filterType];
			image[filterType] = option[rand(option.length)];
		}
		
		image.Like = rand(20);
		image.Comment = rand(image.Like);
		image.Arch = names[rand(names.length)];
		image.Reaction = 'None';
		image.Hot = rand(5) == 1;
		
		if(!image.Hot){
			var r = rand(4);
			if(r == 2)
				image.Reaction = 'Liked';
			if(r == 3)
				image.Reaction = 'Removed';
		}
		else{
			image.Reaction = 'None';
		}
		$scope.images[i-1] = image;
	}

	
	for(var i=0; i<$scope.filterTypes.length; i++){
		var filterType = $scope.filterTypes[i];
		$scope.filters[filterType] = [];
		$scope.filterStatus[filterType] = false;
		$scope.selectedFilter[filterType] = "All " + filterType + "s";
		for(var j=0; j<options[filterType].length; j++){
			$scope.filters[filterType].push(options[filterType][j]);
		}
	}
	


  }])
  .controller('HomeCtrl', ['$scope', function($scope) {

  }])

 	.controller('ProfileCtrl', ['$scope', function($scope){
 		drawChart();
 	}])


  .controller('GalleryCtrl', ['$scope', '$location', '$rootScope', function($scope, $location, $rootScope){
	$rootScope.$on('$locationChangeSuccess', function (e, newUrl) {
		updateFilter();
	});

	function updateFilter(){
		var newUrl = $location.url();
		if(newUrl.indexOf('Favourites') != -1){
			$scope.filterObj.Reaction = 'Liked';
		}
		else{
			$scope.filterObj.Reaction = 'None';
		}
		
		if(newUrl.indexOf('Hot') != -1){
			$scope.filterObj.Hot = true;
		}
		else{
			$scope.filterObj.Hot = undefined;
		}
		
		if($location.$$search.room && $location.$$search.room.length > 0){
			$scope.filterObj.Room = $location.$$search.room;
			$scope.selectedFilter.Room = $location.$$search.room;
		}
		else{
			$scope.selectedFilter.Room = "All Rooms";
			$scope.filterObj.Room = undefined;
		}
		
		if($location.$$search.budget && $location.$$search.budget.length > 0){
			$scope.filterObj.Budget = $location.$$search.budget;
			$scope.selectedFilter.Budget = $location.$$search.budget;
		}
		else{
			$scope.filterObj.Budget = undefined;
			$scope.selectedFilter.Budget = "All Budgets";
		}
		
	}
	
	updateFilter();
	
	$scope.filterSelected = function(filterType, item){
		$scope.selectedFilter[filterType] = item;
		$scope.filterStatus[filterType] = false;
		if(item.indexOf('All')!=-1){
			$scope.filterObj[filterType] = undefined;
		}
		else{
			$scope.filterObj[filterType] = item;
		}
	};
	
	$scope.like = function(image){
		image.Reaction = 'Liked';
		image.Like = image.Like + 1;	
		randomShowHide();	
	};
	
	function randomShowHide(){
		for(var i=0; i<$scope.images.length; i++){
			var image = $scope.images[i];
			if(image.Reaction == 'Removed'){
				if(rand(3) == 1){
					image.Reaction = 'None';
				}
			} else if(image.Reaction == 'None'){
				if(rand(3) == 1){
					image.Reaction = 'Removed';
				}
			}
		}
	}
	
	
	$scope.remove = function(image){
		randomShowHide();
		image.Reaction = 'Removed';
	}
	
}]);
